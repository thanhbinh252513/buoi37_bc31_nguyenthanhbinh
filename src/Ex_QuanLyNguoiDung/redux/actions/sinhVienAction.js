import {
  EDIT_SINH_VIEN,
  THEM_SINH_VIEN,
  UPDATE_SINH_VIEN,
  XOA_SINH_VIEN,
} from "../constant/sinhVienConstant,";

export const themSinhVienAction = (newSinhVien) => ({
  type: THEM_SINH_VIEN,
  newSinhVien,
});
export const xoaSinhVienAction = (sinhVienId) => ({
  type: XOA_SINH_VIEN,
  sinhVienId,
});
export const editSinhVienAction = (sinhVien) => ({
  type: EDIT_SINH_VIEN,
  sinhVien,
});
export const updateSinhVienAction = (sinhVienId, newSinhVien) => ({
  type: UPDATE_SINH_VIEN,
  sinhVienId,
  newSinhVien,
});

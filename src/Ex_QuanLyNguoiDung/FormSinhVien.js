import React, { Component } from "react";
import { connect } from "react-redux";
import {
  themSinhVienAction,
  updateSinhVienAction,
} from "./redux/actions/sinhVienAction";

class FormSinhVien extends Component {
  state = {
    maSV: "",
    hoTen: "",
    email: "",
    soDienThoai: "",
  };
  // handleChange = (e) => {
  //   // Lấy giá trị mỗi lần value input thay đổi bởi người dùng
  //   let tagInput = e.target;
  //   let { name, value, type, pattern } = tagInput;
  //   let errorMessage = "";
  //   // Kiểm tra bất kì control input nào người dùng nhập đều ktra rỗng
  //   if (value.trim() === "") {
  //     errorMessage = name + " không được bỏ trống !";
  //   }
  //   // Kiểm tra email
  //   if (type === "email") {
  //     const regex = new RegExp(pattern);
  //     if (!regex.test(value)) {
  //       errorMessage = name + " không đúng định dạng ";
  //     }
  //   }
  //   if (name === "soDienThoai") {
  //     const regex = new RegExp(pattern);
  //     if (!regex.test(value)) {
  //       errorMessage = name + " không đúng định dạng ";
  //     }
  //   }
  //   let values = { ...this.state.values, [name]: value }; // Cập nhật giá trị values cho state
  //   let errors = { ...this.state.errors, [name]: errorMessage }; // Cập nhật lỗi cho state
  //   this.setState({ ...this.state, values: values, errors: errors }, () => {
  //     console.log(this.state);
  //     this.checkValid();
  //   });
  // };
  // checkValid = () => {
  //   let valid = true;
  //   for (let key in this.state.errors) {
  //     if (this.state.errors[key] !== "" && this.state.values[key] !== "") {
  //       valid = false;
  //     }
  //   }
  //   this.setState({
  //     ...this.state,
  //     valid: valid,
  //   });
  // };
  render() {
    return (
      <div>
        <div className="container">
          <div className="card text-left">
            <div className="card-header bg-dark text-white">
              <h3>Thông tin sinh viên </h3>
            </div>
            <div class="card-body">
              <form>
                <div className="row">
                  <div className="form-group col-6">
                    <span>Mã SV</span>
                    {this.props.disabled ? (
                      <input
                        disabled
                        type="text"
                        className="form-control"
                        name="maSV"
                        value={this.state.maSV}
                        onChange={(e) => {
                          this.setState({
                            maSV: e.target.value,
                          });
                        }}
                      />
                    ) : (
                      <input
                        type="text"
                        className="form-control"
                        name="maSV"
                        value={this.state.maSV}
                        onChange={(e) => {
                          this.setState({
                            maSV: e.target.value,
                          });
                        }}
                      />
                    )}
                  </div>
                  <div className="form-group col-6">
                    <span>Họ tên</span>
                    <input
                      type="text"
                      className="form-control"
                      name="hoTen"
                      value={this.state.hoTen}
                      onChange={(e) => {
                        this.setState({
                          hoTen: e.target.value,
                        });
                      }}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="form-group col-6">
                    <span>Email</span>
                    <input
                      type="email"
                      pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                      className="form-control"
                      name="email"
                      value={this.state.email}
                      onChange={(e) => {
                        this.setState({
                          email: e.target.value,
                        });
                      }}
                    />
                  </div>
                  <div className="form-group col-6">
                    <span>Số điện thoại</span>
                    <input
                      type="text"
                      pattern="^(0|[1-9][0-9]*)$"
                      className="form-control"
                      name="soDienThoai"
                      value={this.state.soDienThoai}
                      onChange={(e) => {
                        this.setState({
                          soDienThoai: e.target.value,
                        });
                      }}
                    />
                  </div>
                </div>
                <button
                  className="btn btn-success"
                  type="button"
                  onClick={() => {
                    // Lấy thông tin người dùng nhập từ input
                    let { maSV, hoTen, email, soDienThoai } = this.state;
                    let newSinhVien = {
                      maSV: maSV,
                      hoTen: hoTen,
                      email: email,
                      soDienThoai: soDienThoai,
                    };
                    this.setState({
                      
                    })
                    this.props.dispatch(themSinhVienAction(newSinhVien));
                    // console.log(maSV, hoTen, email, soDienThoai);
                  }}
                >
                  Thêm sinh viên
                </button>
                <button
                  className="btn btn-primary"
                  type="button"
                  onClick={() => {
                    let { maSV, hoTen, email, soDienThoai } = this.state;
                    let newSinhVien = {
                      maSV: maSV,
                      hoTen: hoTen,
                      email: email,
                      soDienThoai: soDienThoai,
                    };

                    this.props.dispatch(
                      updateSinhVienAction(newSinhVien.maSV, newSinhVien)
                    );
                  }}
                >
                  Cập nhật
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.sinhVienEdited.maSV !== this.props.sinhVienEdited.maSV) {
      this.setState({
        maSV: this.props.sinhVienEdited.maSV,
        hoTen: this.props.sinhVienEdited.hoTen,
        email: this.props.sinhVienEdited.email,
        soDienThoai: this.props.sinhVienEdited.soDienThoai,
      });
    }
  }
}

let mapStateToProps = (state) => {
  return {
    mangSinhVien: state.managerReducer.mangSinhVien,
    sinhVienEdited: state.managerReducer.sinhVienEdited,
    disabled: state.managerReducer.disabled,
  };
};

export default connect(mapStateToProps)(FormSinhVien);

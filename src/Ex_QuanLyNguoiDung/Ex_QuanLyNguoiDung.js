import React, { Component } from "react";
import FormSinhVien from "./FormSinhVien";
import TableSinhVien from "./TableSinhVien";

export default class Ex_QuanLyNguoiDung extends Component {
  render() {
    return (
      <div className="container py-5">
        <h3>Bài tập form </h3>
        <FormSinhVien />
        <TableSinhVien />
      </div>
    );
  }
}

import React, { Component } from "react";
import { connect } from "react-redux";
import {
  editSinhVienAction,
  xoaSinhVienAction,
} from "./redux/actions/sinhVienAction";

class TableSinhVien extends Component {
  renderSinhVien = () => {
    const { mangSinhVien } = this.props;
    return mangSinhVien.map((sinhVien, index) => {
      return (
        <tr key={index}>
          <td>{sinhVien.maSV}</td>
          <td>{sinhVien.hoTen}</td>
          <td>{sinhVien.soDienThoai}</td>
          <td>{sinhVien.email}</td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => {
                this.props.dispatch(xoaSinhVienAction(sinhVien.maSV));
              }}
            >
              Xóa
            </button>
            <button
              className="btn btn-warning"
              onClick={() => {
                this.props.dispatch(editSinhVienAction(sinhVien));
              }}
            >
              Sửa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="pt-5">
        <table className="table">
          <thead>
            <tr className="bg-dark text-white">
              <th>Mã sinh viên</th>
              <th>Tên sinh viên</th>
              <th>Số điện thoại</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>{this.renderSinhVien()}</tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    mangSinhVien: state.managerReducer.mangSinhVien,
    disabled: state.managerReducer.disabled,
  };
};

export default connect(mapStateToProps)(TableSinhVien);
